<?php

// phpcs:ignoreFile

$settings['config_sync_directory'] = 'config/sync';

// Ruta al archivo de hash salt
$salt_file_path = __DIR__ . '/salt.txt';
// Verifica si el archivo salt.txt existe; si no, lo crea con un hash aleatorio
if (!file_exists($salt_file_path)) {
    // Genera un nuevo hash_salt y guarda en salt.txt
    $new_hash_salt = bin2hex(random_bytes(55)); // Genera un hash de 110 caracteres
    file_put_contents($salt_file_path, $new_hash_salt);
}
// Carga el hash_salt desde salt.txt
$settings['hash_salt'] = trim(file_get_contents($salt_file_path));



$settings['update_free_access'] = FALSE;

$settings['file_chmod_directory'] = 0755;
$settings['file_chmod_file'] = 0644;

$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';

$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings['entity_update_batch_size'] = 50;

$settings['entity_update_backup'] = TRUE;

$settings['migrate_node_migrate_type_classic'] = FALSE;

$settings['state_cache'] = TRUE;

if (file_exists($app_root . '/' . $site_path . '/settings.production.php')) {
  include $app_root . '/' . $site_path . '/settings.production.php';
}

if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

// Automatically generated include for settings managed by ddev.
$ddev_settings = dirname(__FILE__) . '/settings.ddev.php';
if (getenv('IS_DDEV_PROJECT') == 'true' && is_readable($ddev_settings)) {
  require $ddev_settings;
}
