# [1.2.5](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.2.4...1.2.5) (2020-09-28)

* fix: required fix_layout_builder

## [1.2.4](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.2.3...v1.2.4) (2020-09-27)


### Bug Fixes

* **styles:** No padding in mobile devices ([624b153](https://gitlab.com/carcheky/druparcheky_theme/commit/624b153bdaa2f48181b7c77c578c0dbd25e8a689))



## [1.2.3](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.2.2...v1.2.3) (2020-09-27)


### Bug Fixes

* **styles:** search bar is not styled properly. ([4889d14](https://gitlab.com/carcheky/druparcheky_theme/commit/4889d14b30e7b04b9e309f49368665032bfb6828))



## [1.2.2](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.2.1...v1.2.2) (2020-09-27)


### Bug Fixes

* **styles:** compiled fix_layout_builder.css ([60bd6b0](https://gitlab.com/carcheky/druparcheky_theme/commit/60bd6b070e13436b74d9afbeeadf78d4ffa8f4a1))



## [1.2.1](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.2.0...v1.2.1) (2020-09-27)


### Bug Fixes

* **styles:** Contact us page styling. ([1a82a3c](https://gitlab.com/carcheky/druparcheky_theme/commit/1a82a3c48903367058cbaae5e70c7b5933b16cb6))
* **styles:** move .js-text-full to base.scss ([4c3dcc9](https://gitlab.com/carcheky/druparcheky_theme/commit/4c3dcc9c03ee6c70138e21beb23d4fefcc5e28f5))



# [1.2.0](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.1.8...v1.2.0) (2020-09-27)


### Features

* drupal 9 support ([8ac1e23](https://gitlab.com/carcheky/druparcheky_theme/commit/8ac1e23d1ab02e3c3acec73ba8394169637158da))



## [1.1.8](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.1.7...v1.1.8) (2020-09-27)


### Bug Fixes

* added dependencies ([5de791f](https://gitlab.com/carcheky/druparcheky_theme/commit/5de791feb85e3c7b4e4bc9e2917c71b372b45792))



## [1.1.7](https://gitlab.com/carcheky/druparcheky_theme/compare/v1.1.6...v1.1.7) (2020-09-27)


### Bug Fixes

* Body overflow when link text too long. ([6ca958d](https://gitlab.com/carcheky/druparcheky_theme/commit/6ca958d93865757b2a4c361e0238b6d0287aefb0))
* **styles:** Styling issue on contact us page ([3c8a820](https://gitlab.com/carcheky/druparcheky_theme/commit/3c8a82039446a4a307f3e63122ec1e6b0b9f6b54))
