# druparcheky_theme

Theme for quick dev

## TWIG vars

- {{ language }} - Devuelve el código de idioma correspondiente. Por defecto añadico como clase al body como 'lang-es'
- {{ nid }} - Devuelve el id del nodo. Por defecto aplicado al body como 'page-1'


## Important files

- druparcheky_subtheme/SCSS folder
- druparcheky_subtheme/Templates folder
- druparcheky_subtheme/js/global.js file
- druparcheky_subtheme/druparcheky_subtheme.info.yml
- druparcheky_subtheme/druparcheky_subtheme.theme
- druparcheky_subtheme/druparcheky_subtheme.layouts.yml
- druparcheky_subtheme/druparcheky_subtheme.libraries.yml


## Install with composer

````bash
composer require drupal/druparcheky_theme
````
or

````bash
composer require carcheky/druparcheky_theme
````
