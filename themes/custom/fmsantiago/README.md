# fmsantiago

Theme for quick dev

## TWIG vars

- {{ language }} - Devuelve el código de idioma correspondiente. Por defecto añadico como clase al body como 'lang-es'
- {{ nid }} - Devuelve el id del nodo. Por defecto aplicado al body como 'page-1'


## Important files

- fmsantiago/SCSS folder
- fmsantiago/Templates folder
- fmsantiago/js/global.js file
- fmsantiago/fmsantiago.info.yml
- fmsantiago/fmsantiago.theme
- fmsantiago/fmsantiago.layouts.yml
- fmsantiago/fmsantiago.libraries.yml


## Install with composer

````bash
composer require drupal/druparcheky_theme
````
or

````bash
composer require carcheky/druparcheky_theme
````
