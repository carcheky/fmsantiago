(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {

      $('[data-fancybox="gallery"]').fancybox({
        // What buttons should appear in the top right corner.
        // Buttons will be created using templates from `btnTpl` option
        // and they will be placed into toolbar (class="fancybox-toolbar"` element)
        buttons: [
          // "zoom",
          //"share",
          // "slideShow",
          //"fullScreen",
          //"download",
          "thumbs",
          "close"
        ],
        protect: true,
        thumbs: {
          autoStart: true, // Display thumbnails on opening
          hideOnClose: true, // Hide thumbnail grid when closing animation starts
          parentEl: ".fancybox-container", // Container is injected into this element
          axis: "x" // Vertical (y) or horizontal (x) scrolling
        },
        image: {
          // Wait for images to load before displaying
          //   true  - wait for image to load and then display;
          //   false - display thumbnail and load the full-sized image over top,
          //           requires predefined image dimensions (`data-width` and `data-height` attributes)
          preload: true
        },

      });
    }
  };
})(jQuery, Drupal);
