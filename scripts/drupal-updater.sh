#!/bin/bash

# Asegúrate de que ./vendor/bin está en el PATH
export PATH="./vendor/bin:$PATH"

# # Fuerza que el entorno use el Composer correcto
[ -f /usr/local/bin/composer2 ] && export COMPOSER_BIN="/usr/local/bin/composer2"

# # Ejecuta composer como php explícitamente para evitar conflictos
# export COMPOSER_BINARY="/usr/bin/php /usr/local/bin/composer2"

# Ejecuta drupal-updater con depuración
drupal-updater update -vvv
composer update -W
git add . && git commit -m "chore: big updates" && git push && drush deploy