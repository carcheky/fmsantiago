#!/bin/bash

current_folder=$(pwd)
[ -f /usr/local/bin/ddev ] && ddev exec bash scripts/pre-cmd.sh && exit 0

chmod 755 sites/default
chmod 755 sites/default/se*

echo "----------------------------"
if [ "$current_folder" != "/var/www/html" ]; then
    echo "------- estás en PROD ------"
    git fetch origin production
    git reset --hard origin/production
    rm -fr config
    git restore config
else
    echo "------- estás en DEV -------"
fi
echo "----------------------------"
