#!/bin/bash

mkdir -p build/.ddev
cp .ddev/config.yaml build/.ddev/
cp -r scripts build/
cp -r sites build/
cp composer* build/