#!/bin/bash
# Script to quickly create sub-theme.

echo '
+------------------------------------------------------------------------+
| With this script you could quickly create bootstrap_sass sub-theme     |
| In order to use this:                                                  |
| - bootstrap_sass theme (this folder) should be in the contrib folder   |
+------------------------------------------------------------------------+
'
read -p 'The machine name of your custom theme? [e.g. bootstrap_sass_custom]' CUSTOM_BOOTSTRAP_SASS
CUSTOM_BOOTSTRAP_SASS=${CUSTOM_BOOTSTRAP_SASS:-bootstrap_sass_custom}
read -p 'Your theme name ? [e.g. My custom bootstrap_sass_custom]' CUSTOM_BOOTSTRAP_SASS_NAME
CUSTOM_BOOTSTRAP_SASS_NAME=${CUSTOM_BOOTSTRAP_SASS_NAME:-bootstrap_sass_custom}

if [[ ! -e ../../custom ]]; then
    mkdir ../../custom
fi
set -e
cd ../../custom
echo $CUSTOM_BOOTSTRAP_SASS
cp -r ../contrib/bootstrap_sass $CUSTOM_BOOTSTRAP_SASS
cd $CUSTOM_BOOTSTRAP_SASS
for file in *bootstrap_sass.*; do mv $file ${file//bootstrap_sass/$CUSTOM_BOOTSTRAP_SASS}; done
for file in config/*/*bootstrap_sass*.*; do mv $file ${file//bootstrap_sass/$CUSTOM_BOOTSTRAP_SASS}; done

# Remove create_subtheme.sh file, we do not need it in customized subtheme.
rm scripts/create_subtheme.sh

# mv {_,}$CUSTOM_BOOTSTRAP_SASS.theme
grep -Rl bootstrap_sass .|xargs sed -i -e "s/bootstrap_sass/$CUSTOM_BOOTSTRAP_SASS/"
sed -i -e "s/SASS Bootstrap Starter Kit Subtheme/$CUSTOM_BOOTSTRAP_SASS_NAME/" $CUSTOM_BOOTSTRAP_SASS.info.yml
echo "# Check the themes/custom folder for your new sub-theme."