<?php

// phpcs:ignoreFile

$settings['file_private_path'] = 'sites/default/private';
$settings['file_public_path'] = 'sites/default/files';
$settings['file_temporary_path'] = '/tmp';
$settings['config_sync_directory'] = 'config/sync';
$settings['default_content_deploy_content_directory'] = 'config/content';
$settings['hash_salt'] = file_get_contents('salt.txt');
$settings['update_free_access'] = FALSE;
$settings['file_chmod_directory'] = 0755;
$settings['file_chmod_file'] = 0644;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];
$settings['entity_update_batch_size'] = 50;
$settings['entity_update_backup'] = TRUE;
$settings['migrate_node_migrate_type_classic'] = FALSE;
$config["config_split.config_split.production"]["status"] = TRUE;
$config["config_split.config_split.developer"]["status"] = FALSE;

if (file_exists($app_root . '/' . $site_path . '/settings.production.php')) {
  include $app_root . '/' . $site_path . '/settings.production.php';
} elseif (getenv('IS_DDEV_PROJECT') == 'true' && is_readable(dirname(__FILE__) . '/settings.ddev.php')) {
  require dirname(__FILE__) . '/settings.ddev.php';
  $config["config_split.config_split.production"]["status"] = FALSE;
  $config["config_split.config_split.developer"]["status"] = TRUE;
}
